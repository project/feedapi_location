Author: Moses Blumenstielmoses@flamingvan.com
Aggregates feeds and creates locations from them. Lets you link the locations to nodes based on the node id in the feed if it exists. Otherwise it lets you link it based on a different node field. Includes a mapper to map feed items to location fields.
Geocoding is configurable. Allows you to limit geocoding queries on updates so that it only queries when the data has changed.
This is an add on for the FeedAPI module: http://drupal.org/project/feedapi

